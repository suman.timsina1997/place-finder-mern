const bcrypt = require("bcryptjs"); 
const {validationResult} = require("express-validator")
const jwt = require("jsonwebtoken");
const HttpError = require("../models/http-error");
const User = require("../models/user");


const getUsers = async(req, res, next)=>{
    let users;
    try{
        users = await User.find({}, "-password");
    }
    catch(err){
        const error = new HttpError("Could not find users! please try again later", 500)
        return next(error);
    }
    res.json({users : users.map(user => user.toObject({ getters: true}))})
};

const singnup = async(req, res, next)=>{
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        return next(
            new HttpError("Invalid data passed, please check your data!", 422)
        );
    }
    const {name, email , password} = req.body;
    let existingUser;
    try{
     existingUser = await User.findOne({email: email})
    }catch(err){
        const error = new HttpError("Signing up failed, please try again later",500);
        return next(error);
    }

    if(existingUser){
        const error = new HttpError("User already exist with this email, please try another email.",422);
        return next (error);
    }
    let hashedPassword;
    try{
        hashedPassword = await bcrypt.hash(password, 12);
    }catch(err){
        const error = new HttpError("Couldnot create user, please try again later!", 500);
        return next(error);
        
    }


    const createdUser = new User({
        name,
        email,
        image: req.file.path,
        password : hashedPassword,
        places:[]
    })

    try{
        await createdUser.save()
    }
    catch(err){
        const error = new HttpError(
            "Signing up failed, try creating again", 500);
        return next (error);
    };


    let token;
    try{
        token = jwt.sign(
            {userId: createdUser.id, email:createdUser.email}, 
            'SUPERSECRET_DONT_SHARE',
            {expiresIn :"6h"}
            )
    }catch(err){
        const error = new HttpError(
            "Signing up failed, try creating again", 500);
        return next (error); 
    }

    res.status(201).json({userId: createdUser.id, email: createdUser.email, token: token}); 
};

const login = async (req, res, next)=>{

    const {email, password} = req.body;

    let existingUser;
    try{
     existingUser = await User.findOne({email: email})
    }catch(err){
        const error = new HttpError("Logging in failed, please try again later",500);
        return next(error);
    };
    if(!existingUser){
        const error = new HttpError("Email and password does not matched!", 401);
        return next(error);
    }

    let isValidPassword;
    try{
        isValidPassword = await bcrypt.compare(password, existingUser.password)
    }catch(err){
        const error = new Error("Username or Password does not matched!!", 500);
        return next(error);
    }

    if(!isValidPassword){
        const error = new HttpError("Email and password does not matched!", 403);
        return next(error);
    };

    try{
        token = jwt.sign(
            {userId: existingUser.id, email:existingUser.email}, 
            'SUPERSECRET_DONT_SHARE',
            {expiresIn :"5h"}
            )
    }catch(err){
        const error = new HttpError(
            "Logging in failed, try creating again", 500);
        return next (error); 
    }
    res.json({
        userId: existingUser.id,
        email: existingUser.email,
        token: token
    })
};


exports.getUsers = getUsers
exports.singnup = singnup;
exports.login = login; 