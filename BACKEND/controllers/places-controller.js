const {validationResult} = require("express-validator");
const HttpError = require("../models/http-error");
const getCoordsForAddress = require("../util/location");
const mongoose = require("mongoose");
const fs = require("fs");

const Place = require("../models/place")
const User = require("../models/user");



const getPlaceById = async (req, res, next) =>{
    console.log("GET request in places..");
    const placeId = req.params.pid;
    let place
    try{
        place  = await Place.findById(placeId);
    }catch(err){
        const error = new HttpError('something went wrong, could not find a place', 500);
        return next(error);
    }
    if(!place){
        const error =  new HttpError("Couldnot find the place for the provided id.", 404);
        return next (error);
    }
    res.json({place:place.toObject( {getters:true} ) })
}


const getPlacesByUserId =  async(req, res, next)=>{
    const userId = req.params.uid;
    let places;
    try{
        places = await Place.find({ creator : userId });
    }
    catch(err){
        const error = new HttpError("Something went wrong, Could not find a place!", 500);
        return next(error);
    }
    if(!places || places.length === 0){
        const error =  new HttpError("Couldnot find the place for the provided user id.", 404);
        return next (error);
   }
    res.json({place : places.map(place => place.toObject({ getters : true}))});
};

const createPlace = async(req, res, next) =>{
    const errors = validationResult(req);

    if(!errors.isEmpty()){
        next( new HttpError("Invalid data passed, please check your data!", 422));
    }
    const {title, description, address} = req.body;

    const coordinates = await getCoordsForAddress(address);

    const createdPlace = new Place({
        title,
        description,
        address,
        location : coordinates,
        image : req.file.path,
        creator : req.userData.userId
    });
    let user;
    try{
        user = await User.findById(req.userData.userId);
    }
    catch(err){
        const error = new HttpError("Creating place failed, please try again!!!",500);
        return next (error);
    }

    if(!user){
        const error = new HttpError("Could not find user for provided ID!",500);
        return next(error);
    };


    try{
        let sess = await mongoose.startSession();
        sess.startTransaction();
        await createdPlace.save({session : sess});
        user.places.push(createdPlace);
        await user.save({session : sess});
        await sess.commitTransaction();
    }
    catch(err){
        const error = new HttpError(
            "Creating place failed, try creating again", 500
        );
        return next (error);
    };
    res.status(201).json({place : createdPlace});
};

const updatePlaceById = async(req,res, next) =>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return  next(
            HttpError("Invalid data passed, please check your data!", 422)
        ) ;
    }
    const {title, description} = req.body;
    const placeId = req.params.pid;
    let place ;
    try{
        place = await Place.findById(placeId);
    }
    catch(err){
        const error = new HttpError("Cannot update the place, please try agin later", 500);
        return next (error);
    }

    if(place.creator.toString() !==req.userData.userId){
        const error = new HttpError("You are not allowed to edit this place!", 401);
        return next (error);
    }
    place.title = title;
    place.description = description;
    try{
        place.save();
    }
    catch(err){
        const error = new HttpError("Cannot update the place, please try agin later" , 500);
        return next(error);
    }
    res.status(200).json({place:place.toObject( {getters : true} )});

};


const deletePlaceById = async (req, res, next) =>{
    const placeId = req.params.pid;
    try{
        place = await Place.findById(placeId).populate("creator");
    }
    catch(err){
        const error = new HttpError("Cannot delete the place, please try agin later", 500);
        return next (error);
    };

    if(!place){
        const error = new HttpError("Could not find place for given ID", 404);
        return next(error);
    };
    if(place.creator.id !==req.userData.userId){
        const error = new HttpError("You are not allowed to delete this place!", 401);
        return next (error);
    }

    const imagePath = place.image;
    try{
        const sess = await mongoose.startSession();
        sess.startTransaction();
        await place.remove({session:sess});
        place.creator.places.pull(place);
        await place.creator.save({session:sess});
        await sess.commitTransaction();
    }
    catch(err){
        const error = new HttpError("Cannot delete the place, please try agin later", 500);
        return next (error);
    }
    fs.unlink(imagePath, err=>{
        console.log(err);
    });
    res.status(200).json({message : "Place Deleted"});
};

exports.getPlaceById = getPlaceById;
exports.getPlacesByUserId = getPlacesByUserId;
exports.createPlace = createPlace;
exports.updatePlaceById = updatePlaceById;
exports.deletePlaceById = deletePlaceById;